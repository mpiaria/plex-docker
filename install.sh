sudo docker build \
	--rm \
	--tag mpiaria/pms-docker:1.11.3.4803-c40bba82e \
	.

sudo docker run \
	--detach \
	--env TZ="America/Chicago" \
	--env PLEX_CLAIM="claim-htWrx2kxFbrtYFiftTaY" \
	--name plex \
	--network=host \
	--restart=always \
	--volume $HOME/docker/plex/config:/config \
	--volume $HOME/docker/plex/data:/data \
	--volume $HOME/docker/plex/transcode:/transcode \
	mpiaria/pms-docker:1.11.3.4803-c40bba82e
