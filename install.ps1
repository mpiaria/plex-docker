docker build `
    --rm `
    --tag mpiaria/pms-docker:1.7.5.4035-313f93718 `
    .

docker run `
    --detach `
    --env ADVERTISE_IP="https://www.michaelpiaria.com:32400/" `
    --env PLEX_CLAIM="claim-SAn2pEJ7yyqzsUDFUdP7" `
    --env TZ="America/Chicago" `
    --hostname "Iaria's Plex Media Server" `
    --name plex `
    --publish 0.0.0.0:3005:3005/tcp `
    --publish 0.0.0.0:8324:8324/tcp `
    --publish 0.0.0.0:32400:32400/tcp `
    --publish 0.0.0.0:32469:32469/tcp `
    --publish 0.0.0.0:1900:1900/udp `
    --publish 0.0.0.0:32410:32410/udp `
    --publish 0.0.0.0:32412:32412/udp `
    --publish 0.0.0.0:32413:32413/udp `
    --publish 0.0.0.0:32414:32414/udp `
    --restart=always `
    --volume F:/Plex/config:/config `
    --volume F:/Plex/data:/data `
    --volume F:/Plex/transcode:/transcode `
    mpiaria/pms-docker:1.7.5.4035-313f93718
